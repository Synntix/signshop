# SignShop
**SignShop** is a Minecraft plugin which allows you to create simple shops with signs.

**IMPORTANT DISCLAIMER**: Enchanted items are **not supported** yet !

## Features
### Implemented

- Simple and lightweight
- Creation and deletion of a shop with a sign
- GUI when buying

### Soon™ implemented

- Support for enchanted items
- Admin shops
- Autocreation of shops

## How to create a shop

Place the item you sell in the first slot of the chest, and the item you request in the second.

![Content of the chest](pictures/chestInventory.png)

Then place a sign against this chest and fill it with:
- 1st row : "\[Shop\]"
- 2nd row : amount of items you are selling
- 3rd row : amount of items you are requesting

![Filled sign](pictures/sign.png)


## Buy an item

Right click on the shop sign, a GUI pops up

![Buying interface](pictures/buyingGUI.png)

Click on the bamboo and buy your precious items

## Build
### Requirements

- Maven

### Instructions
1. Clone project from gitlab: `git clone https://gitlab.com/Synntix/signshop.git`
2. Create the .jar with `mvn clean package`
3. The plugin is now in the target folder.
