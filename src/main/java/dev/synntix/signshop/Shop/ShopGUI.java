package dev.synntix.signshop.Shop;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Objects;

public class ShopGUI implements InventoryHolder {

    private final Inventory inventory;
    private final Shop shop;

    public ShopGUI(ItemStack[] items, Shop shop) {
        String ownerName = Objects.requireNonNull(Bukkit.getPlayer(shop.getOwner())).getDisplayName();
        this.inventory = Bukkit.createInventory(this, 9, ownerName + "'s shop");
        this.shop = shop;
        ItemStack validationItem = new ItemStack(Material.BAMBOO, 1);
        renameItem(validationItem, ChatColor.GREEN + "Buy");

        this.inventory.setItem(2, items[0]);
        this.inventory.setItem(4, validationItem);
        this.inventory.setItem(6, items[1]);
    }

    @Override
    public Inventory getInventory() {
        return this.inventory;
    }

    private void renameItem(ItemStack item, String name) {
        ItemMeta m = item.getItemMeta();
        Objects.requireNonNull(m).setDisplayName(name);
        item.setItemMeta(m);
    }

    public Shop getShop() {
        return shop;
    }
}
