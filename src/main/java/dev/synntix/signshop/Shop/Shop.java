package dev.synntix.signshop.Shop;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Chest;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.Objects;
import java.util.UUID;

public class Shop {

    private final UUID owner;
    private final Location chestLocation;
    private final boolean isAdminShop;
    private final int itemSellingAmount;
    private final int itemPayingAmount;
    private final Material itemSelling;
    private final Material itemPaying;

    public Shop(UUID owner, Location chestLocation, boolean isAdminShop, int itemSellingAmount, int itemPayingAmount, Material itemSelling, Material itemPaying) {
        this.owner = owner;
        this.chestLocation = chestLocation;
        this.isAdminShop = isAdminShop;
        this.itemSellingAmount = itemSellingAmount;
        this.itemPayingAmount = itemPayingAmount;
        this.itemSelling = itemSelling;
        this.itemPaying = itemPaying;
    }

    public UUID getOwner() {
        return owner;
    }

    public ItemStack[] getTwoFirstItems() {
        ItemStack itemSelling = new ItemStack(this.itemSelling, this.itemSellingAmount);
        ItemStack itemPaying = new ItemStack(this.itemPaying, this.itemPayingAmount);
        return new ItemStack[]{itemSelling, itemPaying};
    }

    public Location getLocation() {
        return chestLocation;
    }

    public void doTransaction(UUID buyerUUID) {
        Player buyer = Bukkit.getPlayer(buyerUUID);
        Chest chest = (Chest) chestLocation.getBlock().getState();
        Inventory chestInventory = chest.getBlockInventory();
        ItemStack itemStackSelling = new ItemStack(itemSelling, itemSellingAmount);
        ItemStack itemStackPaying = new ItemStack(itemPaying, itemPayingAmount);

        if (isAdminShop) {
            Objects.requireNonNull(buyer).getInventory().removeItem(itemStackPaying);
            buyer.getInventory().addItem(itemStackSelling);
        } else {
            if (!chestInventory.addItem(itemStackPaying).isEmpty()) {
                Objects.requireNonNull(buyer).sendMessage(ChatColor.RED + "This chest is full, it can't receive your payment...");
            } else {
                chestInventory.removeItem(itemStackSelling);
                Objects.requireNonNull(buyer).getInventory().removeItem(itemStackPaying);
                buyer.getInventory().addItem(itemStackSelling);
            }
        }
    }

    public Chest getChest() {
        return (Chest) chestLocation.getBlock().getState();
    }

    public int getItemSellingAmount() {
        return itemSellingAmount;
    }

    public int getItemPayingAmount() {
        return itemPayingAmount;
    }

    public Material getItemSelling() {
        return itemSelling;
    }

    public Material getItemPaying() {
        return itemPaying;
    }

    public boolean isAdminShop() {
        return isAdminShop;
    }
}
