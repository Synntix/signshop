package dev.synntix.signshop;

import dev.synntix.signshop.Shop.Shop;
import dev.synntix.signshop.Shop.ShopGUI;
import dev.synntix.signshop.managers.ShopsManager;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.block.Sign;
import org.bukkit.block.data.type.WallSign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.inventory.InventoryMoveItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import java.util.Objects;

public class Events implements Listener {

    private final ShopsManager shopsManager;

    public Events(ShopsManager shopsManager) {
        this.shopsManager = shopsManager;
    }

    @EventHandler
    public void onBlockPlaced(BlockPlaceEvent e) {
        if (!isWallSign(e.getBlockPlaced().getType())) {
            return;
        }

        if (e.getBlockAgainst().getType() != Material.CHEST) {
            return;
        }

        if (e.getBlockPlaced().getState() instanceof Sign) {
            // TODO: Autocreate the shop
        }
    }

    @EventHandler
    public void onSignUpdate(SignChangeEvent e) {
        if (!isWallSign(e.getBlock().getType())) {
            return;
        }

        Player player = e.getPlayer();
        WallSign signData = (WallSign) e.getBlock().getBlockData();
        Block attachedBlock = e.getBlock().getRelative(signData.getFacing().getOppositeFace());

        if (attachedBlock.getState() instanceof Chest) {
            Chest chest = (Chest) attachedBlock.getState();
            if (e.getLine(0) != null && Objects.requireNonNull(e.getLine(0)).equals("[Shop]")) {
                if (isNumeric(e.getLine(1)) && isNumeric(e.getLine(2))) {
                    ItemStack[] chestContents = chest.getBlockInventory().getStorageContents();
                    int itemSellingAmount = Integer.parseInt(Objects.requireNonNull(e.getLine(1)));
                    int itemPayingAmount = Integer.parseInt(Objects.requireNonNull(e.getLine(2)));
                    if (chestContents[0] != null && chestContents[1] != null) {
                        ItemStack itemSelling = chestContents[0];
                        ItemStack itemPaying = chestContents[1];
                        shopsManager.createShop(player.getUniqueId(), chest.getLocation(),
                                false, itemSellingAmount, itemPayingAmount,
                            itemSelling.getType(), itemPaying.getType());
                        e.setLine(0, ChatColor.BLUE + e.getLine(0));
                        e.setLine(1, ChatColor.GREEN + e.getLine(1));
                        e.setLine(2, ChatColor.RED + e.getLine(2));
                        player.sendMessage(ChatColor.GREEN + "Your shop has been created.");
                    } else {
                        player.sendMessage(ChatColor.RED + "You must put items on the two first slots.");
                    }
                } else {
                    player.sendMessage(ChatColor.RED + "Wrong sign format.");
                }
            }
        }
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent e) {
        if (e.getClickedBlock() == null) {
            return;
        }

        Player player = e.getPlayer();

        if (isWallSign(e.getClickedBlock().getType())) {
            Sign sign = (Sign) e.getClickedBlock().getState();
            Shop shop = shopsManager.getShopWithSign(sign);
            if (shop != null && e.getAction() == Action.RIGHT_CLICK_BLOCK) {
                ShopGUI gui = new ShopGUI(shop.getTwoFirstItems(), shop);
                player.openInventory(gui.getInventory());
            }

        } else if (e.getClickedBlock().getType() == Material.CHEST) {
            Shop shop = shopsManager.getShop(e.getClickedBlock().getLocation());
            if (shop != null) {
                if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
                    if (!shop.getOwner().equals(player.getUniqueId())) {
                        e.setCancelled(true);
                        ShopGUI gui = new ShopGUI(shop.getTwoFirstItems(), shop);
                        player.openInventory(gui.getInventory());
                    }
                }
            }
        }
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent e) {
        Block block = e.getBlock();
        Player player = e.getPlayer();

        if (isWallSign(block.getType())) {
            Sign sign = (Sign) block.getState();
            Shop shop = shopsManager.getShopWithSign(sign);

            if (shop != null) {
                if (!shop.getOwner().equals(player.getUniqueId())) {
                    e.setCancelled(true);
                    player.sendMessage(ChatColor.RED + "This is not your shop.");
                } else {
                    shopsManager.deleteShop(shop);
                }

            }

        }
    }

    @EventHandler
    public void onInventoryMoveItem(InventoryMoveItemEvent e) {
        Location sourceLocation = e.getSource().getLocation();
        Location destinationLocation = e.getDestination().getLocation();

        if (shopsManager.getShop(sourceLocation) != null || shopsManager.getShop(destinationLocation) != null) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onChestListClick(InventoryClickEvent e) {
        Player player = (Player) e.getWhoClicked();
        if (e.getInventory().getHolder() instanceof ShopGUI) {
            e.setCancelled(true);
            ItemStack item = e.getCurrentItem();
            if (item != null) {
                if (item.getItemMeta() != null && item.getItemMeta().getDisplayName().equals(ChatColor.GREEN + "Buy")) {
                    Shop shop = ((ShopGUI) e.getInventory().getHolder()).getShop();
                    PlayerInventory playerInventory = e.getWhoClicked().getInventory();
                    if (shop.getChest().getBlockInventory().contains(shop.getItemSelling(), shop.getItemSellingAmount())) {
                        if (playerInventory.contains(shop.getItemPaying(), shop.getItemPayingAmount())) {
                            shop.doTransaction(player.getUniqueId());
                        } else {
                            player.sendMessage(ChatColor.RED + "You don't have enough " + shop.getItemPaying().toString().toLowerCase());
                        }
                    } else {
                        player.sendMessage(ChatColor.RED + "This shop is out of stock.");
                    }
                }
            }
        }
    }

    // Cancel dragging in our inventory
    @EventHandler
    public void onInventoryClick(final InventoryDragEvent e) {
        if (e.getInventory().getHolder() instanceof ShopGUI) {
            e.setCancelled(true);
        }
    }


    private boolean isWallSign(Material m) {
        return m == Material.SPRUCE_WALL_SIGN
                || m == Material.ACACIA_WALL_SIGN
                || m == Material.BIRCH_WALL_SIGN
                || m == Material.CRIMSON_WALL_SIGN
                || m == Material.JUNGLE_WALL_SIGN
                || m == Material.WARPED_WALL_SIGN
                || m == Material.DARK_OAK_WALL_SIGN
                || m == Material.OAK_WALL_SIGN;
    }

    public static boolean isNumeric(String str) {
        try {
            Integer.parseInt(str);
            return true;
        } catch(NumberFormatException e){
            return false;
        }
    }

}
