package dev.synntix.signshop.managers;

import dev.synntix.signshop.Shop.Shop;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.block.Sign;
import org.bukkit.block.data.type.WallSign;

import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class ShopsManager {

    private final DataManager dataManager;
    private final ConcurrentMap<Location, Shop> shopsList;

    public ShopsManager(DataManager dataManager) {
        this.dataManager = dataManager;
        this.shopsList = new ConcurrentHashMap<>(dataManager.getSavedShops());
    }

    public Shop getShop(Location location) {
        return this.shopsList.get(location);
    }

    public void createShop(UUID owner, Location chestLocation, boolean isAdminShop, int itemSellingAmount, int itemPayingAmount, Material itemSelling, Material itemPaying) {
        Shop shop = new Shop(owner, chestLocation, isAdminShop, itemSellingAmount, itemPayingAmount, itemSelling, itemPaying);
        shopsList.put(chestLocation, shop);
        dataManager.saveShop(shop);
    }

    public Shop getShopWithSign(Sign sign) {
        WallSign signData = (WallSign) sign.getBlockData();
        Block attachedBlock = sign.getBlock().getRelative(signData.getFacing().getOppositeFace());
        if (attachedBlock.getState() instanceof Chest) {
            Chest chest = (Chest) attachedBlock.getState();
            return this.shopsList.get(chest.getLocation());
        }

        return null;
    }

    public void deleteShop(Shop shop) {
        Location shopLocation = shop.getLocation();

        shopsList.remove(shopLocation);
        dataManager.removeShop(shop);
    }

}
