package dev.synntix.signshop.managers;

import dev.synntix.signshop.Shop.Shop;
import dev.synntix.signshop.SignShop;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.logging.Level;

public class DataManager {

    private final SignShop plugin;
    private File datafile;
    private YamlConfiguration dataConfig;

    public DataManager(SignShop plugin) {
        this.plugin = plugin;
        createDataFile();
    }

    private void createDataFile() {
        this.datafile = new File(plugin.getDataFolder(), "Shops.yml");

        try {
            if (datafile.createNewFile()) {
                plugin.getLogger().log(Level.INFO, "Creating Shops config file.");
            }
        } catch (Exception e) {
            plugin.getLogger().log(Level.SEVERE, "An error occurred while attempting to create Shops config file.");
            e.printStackTrace();
        }

        this.dataConfig = YamlConfiguration.loadConfiguration(datafile);
    }

    public void saveData() {
        try {
            dataConfig.save(datafile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void saveShop(Shop shop) {
        if (shop.getLocation().getWorld() != null) {
            String world = shop.getLocation().getWorld().getName();
            int x = shop.getLocation().getBlockX();
            int y = shop.getLocation().getBlockY();
            int z = shop.getLocation().getBlockZ();

            dataConfig.set(world + "." + x + "_" + y +"_" + z + ".playerUUID", shop.getOwner().toString());
            dataConfig.set(world + "." + x + "_" + y +"_" + z + ".adminShop", shop.isAdminShop());
            dataConfig.set(world + "." + x + "_" + y +"_" + z + ".itemSellingAmount", shop.getItemSellingAmount());
            dataConfig.set(world + "." + x + "_" + y +"_" + z + ".itemPayingAmount", shop.getItemPayingAmount());
            dataConfig.set(world + "." + x + "_" + y +"_" + z + ".itemSelling", shop.getItemSelling().toString());
            dataConfig.set(world + "." + x + "_" + y +"_" + z + ".itemPaying", shop.getItemPaying().toString());
            saveData();

        }
    }

    public void removeShop(Shop shop) {
        Location location = shop.getLocation();
        String worldString = Objects.requireNonNull(location.getWorld()).getName();
        int x = location.getBlockX();
        int y = location.getBlockY();
        int z = location.getBlockZ();

        dataConfig.set(worldString + "." + x + "_" + y + "_" + z, null);
        saveData();
    }

    public ConcurrentMap<Location, Shop> getSavedShops() {
        ConcurrentMap<Location, Shop> shopsList = new ConcurrentHashMap<>();

        for (String worldString : dataConfig.getKeys(false)) {
            for (String coordsString : Objects.requireNonNull(dataConfig.getConfigurationSection(worldString)).getKeys(false)) {
                World world = plugin.getServer().getWorld(worldString);

                String[] coords = coordsString.split("_");

                int x = Integer.parseInt(coords[0]);
                int y = Integer.parseInt(coords[1]);
                int z = Integer.parseInt(coords[2]);

                Location location = new Location(world, x, y, z);


                UUID playerUUID = UUID.fromString(Objects.requireNonNull(dataConfig.getString(worldString + "." + coordsString + ".playerUUID")));
                boolean adminShop = Boolean.getBoolean(dataConfig.getString(worldString + "." + coordsString + ".adminShop"));
                int itemSellingAmount = Integer.parseInt(Objects.requireNonNull(dataConfig.getString(worldString + "." + coordsString + ".itemSellingAmount")));
                int itemPayingAmount = Integer.parseInt(Objects.requireNonNull(dataConfig.getString(worldString + "." + coordsString + ".itemPayingAmount")));
                Material itemSelling = Material.getMaterial(Objects.requireNonNull(dataConfig.getString(worldString + "." + coordsString + ".itemSelling")));
                Material itemPaying = Material.getMaterial(Objects.requireNonNull(dataConfig.getString(worldString + "." + coordsString + ".itemPaying")));

                Shop shop = new Shop(playerUUID, location, adminShop, itemSellingAmount, itemPayingAmount, itemSelling, itemPaying);

                shopsList.put(location, shop);

            }
        }
        saveData();

        return shopsList;
    }
}
