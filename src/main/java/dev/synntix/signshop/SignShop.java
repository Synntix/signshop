package dev.synntix.signshop;

import dev.synntix.signshop.managers.DataManager;
import dev.synntix.signshop.managers.ShopsManager;
import org.bukkit.plugin.java.JavaPlugin;

public final class SignShop extends JavaPlugin {

    @Override
    public void onEnable() {
        // Plugin startup logic
        DataManager dataManager = new DataManager(this);
        ShopsManager shopsManager = new ShopsManager(dataManager);
        getServer().getPluginManager().registerEvents(new Events(shopsManager), this);

    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }
}
